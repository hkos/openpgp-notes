# Notes on OpenPGP

Documentation around OpenPGP, RFC 4880 etc.

This project is superseded by https://openpgp.dev/

Sources for openpgp.dev are on: https://codeberg.org/openpgp/notes/